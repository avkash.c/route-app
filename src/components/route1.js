import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import Portfolio from "./portfolio";

const Pages = () => {
  const { path, url } = useRouteMatch();

  const userHandler = e => {
    const HOST = window.location.hostname;
    const PORT = window.location.port;
    console.log(e.target.user.value);
    console.log(`${HOST}:${PORT}${url}/myPortfolio/${e.target.user.value}`);
    e.preventDefault();
    window.location.href = `${url}/myPortfolio/${e.target.user.value}`;
  };
  return (
    <Router>
      <Switch>
        <Route path={`${path}/myPortfolio/:user`}>
          <Portfolio />
        </Route>
        <Route path={`${path}/myPortfolio/`}>
          <Portfolio userHandler={userHandler} />
        </Route>
        <Route exact path={path}>
          <h2>
            <h3>Welcome to page</h3>
            <p>Go to</p>
            <Link to={`${url}/myPortfolio`}>
              url: {url} <br /> path: {path}
            </Link>
          </h2>
          {/* <Portfolio /> */}
        </Route>
      </Switch>
    </Router>
  );
};

export default Pages;
