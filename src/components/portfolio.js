import React, { useState } from "react";
import { useParams, useRouteMatch } from "react-router-dom";

const Portfolio = ({ userHandler }) => {
  let { user } = useParams();
  let { url, path } = useRouteMatch();
  const [val, setVal] = useState("");
  console.log(user);

  return (
    <div>
      {!user ? (
        <form onSubmit={userHandler}>
          <h2>Welcome Guest</h2>
          <input
            name="user"
            type="text"
            value={val}
            onChange={e => {
              setVal(e.target.value);
            }}
          />
          <button type="submit">Go</button>
        </form>
      ) : (
        <div>
          <h3>
            {url} {path}
          </h3>
          <h2>Welcome {user}</h2>
          <h4>This is your portfolio</h4>
        </div>
      )}
    </div>
  );
};

export default Portfolio;
