import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const Shop = () => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetchItems();
  }, []);

  const fetchItems = async () => {
    const data = await fetch("https://jsonplaceholder.typicode.com/posts");
    const items = await data.json();
    console.log(items);
    setItems(items);
  };

  return (
    <div>
      {items.map(item => {
        return (
          <h2 key={item.id}>
            <Link to={`/shop/${item.id}`}>{item.title}</Link>
          </h2>
        );
      })}
    </div>
  );
};

export default Shop;
