import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

const ItemDetails = () => {
  const [item, setItem] = useState({});
  let { id } = useParams();
  useEffect(() => {
    fetchItem();
  }, []);

  const fetchItem = async () => {
    const data = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}`
    );
    const item = await data.json();
    console.log(item);
    setItem(item);
  };

  return (
    <div>
      <h2>Item</h2>
      <p>{item.title}</p>
      <p>{item.body}</p>
    </div>
  );
};

export default ItemDetails;
