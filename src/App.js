import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";
import Portfolio from "./components/portfolio";
import Pages from "./components/route1";
import Shop from "./components/shop";
import ItemDetails from "./components/itemDetails";
import AuthExample from "./components/authRoute";
import CustomLinkExample from "./components/customLink";
import PreventingTransitionsExample from "./components/preventTransfer";
import RecursiveExample from "./components/recursivePath";
import SidebarExample from "./components/sidebar";
import AnimationExample from "./components/animateTransition";

function App() {
  return (
    <Router>
      <div className="App">
        <AnimationExample />
        {/* <SidebarExample /> */}
        {/* <RecursiveExample /> */}
        {/* <PreventingTransitionsExample /> */}
        {/* <CustomLinkExample /> */}
        {/* <AuthExample /> */}
        {/* <Switch>
          <Route path="/page">
            <Pages />
          </Route>
          <Route exact path="/shop">
            <Shop />
          </Route>
          <Route path="/shop/:id">
            <ItemDetails />
          </Route>
          <Route exact path="/">
            <h1>Welcome, got to the '/page'</h1>
          </Route>
        </Switch> */}
      </div>
    </Router>

    // <Router>
    //   <div>
    //     <ul>
    //       <li>
    //         <Link to="/">Home</Link>
    //       </li>
    //       <li>
    //         <Link to="/about">About</Link>
    //       </li>
    //       <li>
    //         <Link to="/topics">Topics</Link>
    //       </li>
    //     </ul>

    //     <Switch>
    //       <Route path="/about">
    //         <About />
    //       </Route>
    //       <Route path="/topics">
    //         <Topics />
    //       </Route>
    //       <Route path="/">
    //         <Home />
    //       </Route>
    //     </Switch>
    //   </div>
    // </Router>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Topics() {
  let match = useRouteMatch();

  return (
    <div>
      <h2>Topics</h2>

      <ul>
        <li>
          <Link to={`${match.url}/components`}>Components</Link>
        </li>
        <li>
          <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
        </li>
      </ul>

      {/* The Topics page has its own <Switch> with more routes
          that build on the /topics URL path. You can think of the
          2nd <Route> here as an "index" page for all topics, or
          the page that is shown when no topic is selected */}
      <Switch>
        <Route path={`${match.path}/:topicId`}>
          <Topic />
        </Route>
        <Route path={match.path}>
          <h3>Please select a topic.</h3>
        </Route>
      </Switch>
    </div>
  );
}

function Topic() {
  let { topicId } = useParams();
  return <h3>Requested topic ID: {topicId}</h3>;
}

export default App;
