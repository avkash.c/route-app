React Router:-

==> Basic:-

    import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

    Every links inside <Router></Router> tags
    give link to change page as <Link to="/myPage">Info</Link>

    The page on which we are directing should be defined as follow
    <Router>
        <Switch>
            <Route path="/myPage">
                <InfoComponent />
            </Route>
            <Route exact path="/">  //'exact' param disables the partial matching for a route and makes sure that it only
                <Home />            //returns the route if the path is an EXACT match to the current url
            </Route>
        <Switch>
    <Router>

    - Switch is used for just route in one tag. if it matches more then one route path, ti will execute only the first route tag. without switch it will load every route path match.

==> Url parameters:-

    Parameters are define using colon (:) in path attribute of Route

    eg. <Route path="/:myParam" children={<Child />}>
    now in Child component

    let {myParam} = useParams(); //useParams should be import at the top.

    return (
        <div>
            Parameter value is {myParam}
        </div>
    )

==> Nesting:-

    It can be done using useRouteMatch().
    eg. In parent
    <Route path="/topics">
        <Topics />
    </Routes>

    now in Topics component

    let {path, url}=useRouteMatch(); //'url' contain matching url

    <Link to={`${url}/newPage`}>newPage</Link>

    <Route path={`${path}/newPage`}>
        <h2>This is new page.</h2>
    </Route>

==> Authenticate Redirect:-

    <Route
        render={({location})=>(     //returning jsx. , location is destructured
            boolValue?(
                <Redirect
                    to={{pathname: '/login', state: {from: location}}}
                />
            ):(
                <Success />
            )
        }
    />

==> Custom link:-

    check with useRouteMatch()

    pass object to match it with current route
    eg. if current url is '/about'

    const match = useRouteMatch({
        path: '/about',
        exact: true
    })

    It will return object with path, url, isExact etc. but

    const match = useRouteMatch({
        path: '/about12',
        exact: true
    })

    It will return null.

    so in example customLink.js {match && "> "} will return according to the value of 'match'

==> Preventing Transition:-

    import { Prompt } from 'react-router-dom'

    <Prompt
        when={isBlocking}
        message={location=>`Are you sure? want to go to ${location.pathname}`}
    />

    when isBlocking is true, it will block transfer and prompt.

==> No Match 404:-

    <Route path="*">
        <NoMatchComponent />
    </Route>

    - Any path that is not defined will be redirecte to <NoMatchComponent />

==> Recursive Path:-

    - Recursive path can be created with dynamic link and path

    eg.
    const {id}=useParam();

    <Link to={`${url}/${id}`}>

    <Route path={`${url}/:id`}>
        <Person />
    </Route>

==> Animated Transition:-

    - import {TransitionGroup, CSSTransition} from "react-transition-group";

    - This is no different than other usage of <CSSTransition>, just make sure to pass `location` to `Switch` so it can match the old location as it animates out.

    - npm install react-transition-group

    - Create file and add below css code to react-router-dom/examples/Animation/styles.css

    .fade-enter {
        opacity: 0;
        z-index: 1;
    }

    .fade-enter.fade-enter-active {
        opacity: 1;
        transition: opacity 250ms ease-in;
    }

    - import "react-router-dom/examples/Animation/styles.css";

    - eg. animateTransition.js
